grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr

    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)

    | if_else NL -> if_else
    | NL ->
    ;

if_else
    : IF^ cond=expr THEN! act1=expr (ELSE! act2=expr)?
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR   :'var' ;
IF    : 'if' ;
ELSE  : 'else' ;
THEN  : 'then' ;
ID    : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
INT   : '0'..'9'+ ;
NL    : '\r'? '\n' ;
WS    : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP    : '(' ;
RP    : ')' ;
PODST : '=' ;
PLUS  : '+' ;
MINUS : '-' ;
MUL   : '*' ;
DIV   : '/' ;
